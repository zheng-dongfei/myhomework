package com.lagou.dao;



import com.lagou.pojo.User;

import java.util.List;

public interface IUserDao {

    //查询所有用户
    public List<User> selectList() throws Exception;


    //根据条件进行用户查询
    public User selectOne(User user) throws Exception;

    //添加用户
    public int insertUser(User user);

    //删除用户
    public int deleteUserById(User user);

    //修改用户信息
    public int updateUserById(User user);




}
