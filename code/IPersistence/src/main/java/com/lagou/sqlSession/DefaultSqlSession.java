package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;
import com.lagou.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {

        //将要去完成对simpleExecutor里的query方法的调用
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        List<Object> list = simpleExecutor.query(configuration, mappedStatement, params);

        return (List<E>) list;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> objects = selectList(statementId, params);
        if(objects.size()==1){
            return (T) objects.get(0);
        }else {
            throw new RuntimeException("查询结果为空或者返回结果过多");
        }

    }

    @Override
    public int insert(String statementId,Object... params) throws Exception {
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        //获取不到 mappedStatement=null
        simpleExecutor.queryX(configuration, mappedStatement,params);

        return 1;
    }

    @Override
    public int delete(String statementId,Object... params) throws Exception {
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        simpleExecutor.queryX(configuration, mappedStatement,params);
        return 1;
    }

    @Override
    public int update(String statementId,Object... params) throws Exception {
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        simpleExecutor.queryX(configuration, mappedStatement,params);
        return 1;
    }


    @Override
    public <T> T getMapper(Class<?> mapperClass) {
        // 使用JDK动态代理来为Dao接口生成代理对象，并返回

        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                // 底层都还是去执行JDBC代码 //根据不同情况，来调用selctList或者selectOne
                // 准备参数 1：statmentid :sql语句的唯一标识：namespace.id= 接口全限定名.方法名
                // 方法名：findAll
                Object result = null;
                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();
                String statementId = className+"."+methodName;
                //获取方法名

                // 准备参数2：params:args
                // 获取被调用方法的返回值类型
                //Type genericReturnType = method.getGenericReturnType();
                // 判断是否进行了 泛型类型参数化
              switch (methodName) {
                  case "selectList": {
                          result=selectList(statementId, args);
                      break;
                  }
                  case "selectOne":{
                      result= selectOne(statementId, args);
                      break;
                  }
                  case "insertUser":{
                      result=insert(statementId,args);
                      break;
                  }
                  case "deleteUserById":{
                      result=delete(statementId,args);
                      break;
                  }
                  case "updateUserById":{
                      result=update(statementId,args);
                  }
                  default:
                      break;
              }

              return result;
            }



        });

        return (T) proxyInstance;
    }



}
